# declare and assign a value to PI
pi = 3.14159
radius = 8

# diameter of circle
diameter = 2 * radius

# area of circle
area = 2 * pi * radius

# circumference of circle
circumference = pi * radius ** 2

print(diameter)
print(area)
print(circumference)
