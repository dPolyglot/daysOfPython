x = 12
y = 13
z = 15

sum = x + y + z
average = sum / 3
product = x * y * z
minimum = min(x, y, z)
maximum = max(x,y,z)

print(sum)
print(average)
print(product)
print(minimum)
print(maximum)