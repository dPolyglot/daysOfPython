from pynput.keyboard import Key, Controller


def turing_test():
    user_first_response = input('What is your problem?')

    keyboard = Controller()
    key = "enter"

    if user_first_response != "" or user_first_response == "":
        if keyboard.pressed(key):
            user_second_response = input('Have you had this problem before (yes or no)?')

            if user_second_response == "yes":
                print('Well, you have it again.')

            if user_second_response == "no":
                print('Well, you have it now.')


turing_test()
